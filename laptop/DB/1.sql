SELECT A.*, C.email as email_konselor, B.foto_konselor, B.lama_pengalaman,
(SELECT SUM(rating)/ COUNT(id) as rating FROM post_rating
WHERE konselorid = A.id_konselor) as rating FROM vw_layanan A
LEFT JOIN konselor_detail B ON A.id_konselor = B.conselor_id
LEFT JOIN register_konselor C ON C.id = B.conselor_id
WHERE 1
AND nama LIKE '%Chat%' AND duration_time = '90' AND nama_kategori LIKE '%Win Self%'