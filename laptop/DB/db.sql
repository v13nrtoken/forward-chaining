-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.14-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table nina.ci_sessions
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT 0,
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table nina.ci_sessions: ~0 rows (approximately)
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_customers
DROP TABLE IF EXISTS `tbl_customers`;
CREATE TABLE IF NOT EXISTS `tbl_customers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `vehicles` text NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `servername` varchar(100) DEFAULT NULL,
  `communication` text NOT NULL,
  `address` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A= Active D= Deactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;

-- Dumping data for table nina.tbl_customers: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_customers` DISABLE KEYS */;
INSERT INTO `tbl_customers` (`id`, `fullname`, `email`, `phone`, `vehicles`, `username`, `servername`, `communication`, `address`, `created_at`, `updated_at`, `status`) VALUES
	(9, 'Anisa Nur', '', '+62 8527318181', 'UP-16 BT-9827\r\nUP-23 T-2973\r\nUP-23 T-7323\r\n', NULL, NULL, 'NIL', 'Gowa', '2020-04-16 17:29:18', '2020-04-13 14:30:55', 'A'),
	(10, 'Cahyati', '', '+62 9412273108', 'UP-81 AF-4013\r\nUP-81 AB-9420\r\nUP-87 9143\r\nUP-81 BT-5488\r\nUP-81 BT-5388\r\nUP-81 AV-4099\r\nUP-81 BP-1313\r\nUP-81 0013\r\nUP-81 CT-3820\r\nUP-81 CT-2537\r\nUP-13 T-2537\r\nUP-81 CT-1300\r\nUP-81 BT-5234\r\nUP-81 BT-5389\r\nUP-81 BJ-0013\r\nUP-81 BT-4496\r\nUP-81 CT-1313\r\n', NULL, NULL, 'NIL', 'Maros', '2020-04-13 14:45:44', '2020-04-13 14:32:19', 'A');
/*!40000 ALTER TABLE `tbl_customers` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_diagnosa
DROP TABLE IF EXISTS `tbl_diagnosa`;
CREATE TABLE IF NOT EXISTS `tbl_diagnosa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `macam_kerusakan` varchar(150) DEFAULT 'Hardware',
  `kode_kerusakan` varchar(50) DEFAULT NULL,
  `jenis_kerusakan` varchar(50) DEFAULT NULL,
  `kode_gejala` varchar(50) DEFAULT NULL,
  `gejala` varchar(250) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT 0,
  `date_created` timestamp NULL DEFAULT NULL,
  `updatedBy` varchar(150) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_gejala` (`kode_gejala`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nina.tbl_diagnosa: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_diagnosa` DISABLE KEYS */;
INSERT INTO `tbl_diagnosa` (`id`, `macam_kerusakan`, `kode_kerusakan`, `jenis_kerusakan`, `kode_gejala`, `gejala`, `isDeleted`, `date_created`, `updatedBy`, `updatedDtm`) VALUES
	(1, 'Hardware', 'K01', 'Keyboard', 'G01', 'Semua tombol tidak berfungsi', 0, '2021-12-06 14:40:38', NULL, NULL),
	(2, 'Hardware', 'K01', 'Keyboard', 'G02', 'Hanya satu atau beberapa tombol yang \r\ntidak berfungsi\r\n', 0, '2021-12-06 14:40:39', NULL, NULL),
	(3, 'Hardware', 'K02', 'Layar', 'G03', 'Layar Monitor Bergaris ketika On', 0, '2021-12-07 13:16:44', NULL, NULL),
	(4, 'Hardware', 'K02', 'Layar', 'G04', 'Layar Redup Ketika On', 0, '2021-12-07 13:16:45', NULL, NULL),
	(5, 'Hardware', 'a', 's', 'p[]', 'sp[][][p[]\\p[]\\p[]\\p[]\\p[]', 1, '2021-12-09 12:12:59', '1', '2021-12-09 05:10:11'),
	(6, 'Hardware', 'tes', 'tes', NULL, 'tes', 1, '2021-12-09 12:13:00', '1', '2021-12-09 05:12:39');
/*!40000 ALTER TABLE `tbl_diagnosa` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_last_login
DROP TABLE IF EXISTS `tbl_last_login`;
CREATE TABLE IF NOT EXISTS `tbl_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `sessionData` varchar(2048) NOT NULL,
  `machineIp` varchar(1024) NOT NULL,
  `userAgent` varchar(128) NOT NULL,
  `agentString` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `createdDtm` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- Dumping data for table nina.tbl_last_login: ~42 rows (approximately)
/*!40000 ALTER TABLE `tbl_last_login` DISABLE KEYS */;
INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
	(1, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-24 15:12:00'),
	(2, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-24 15:52:20'),
	(3, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-24 16:32:10'),
	(4, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-24 17:19:58'),
	(5, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-24 17:50:46'),
	(6, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-25 12:59:34'),
	(7, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-26 11:58:15'),
	(8, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-26 17:32:57'),
	(9, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-27 09:09:58'),
	(10, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-27 11:00:15'),
	(11, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-27 17:11:15'),
	(12, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-28 08:40:34'),
	(13, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '::1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-28 13:48:31'),
	(14, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-29 17:13:57'),
	(15, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-29 21:56:03'),
	(16, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 75.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', '2020-04-30 13:05:42'),
	(17, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 18:23:16'),
	(18, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 18:35:00'),
	(19, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 18:38:39'),
	(20, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 18:40:26'),
	(21, 3, '{"role":"3","roleText":"Employee","name":"Employee"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 19:01:46'),
	(22, 2, '{"role":"2","roleText":"Manager","name":"Manager"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 19:03:27'),
	(23, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 19:28:08'),
	(24, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 19:29:16'),
	(25, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-03 20:01:38'),
	(26, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-06 08:14:07'),
	(27, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-06 12:50:20'),
	(28, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-07 13:11:19'),
	(29, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-07 13:18:28'),
	(30, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-07 17:16:52'),
	(31, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-07 17:32:26'),
	(32, 3, '{"role":"3","roleText":"Employee","name":"Employee"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-07 17:35:31'),
	(33, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-07 18:58:24'),
	(34, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 94.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0', 'Windows 10', '2021-12-08 11:22:24'),
	(35, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 95.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', 'Windows 10', '2021-12-08 17:39:03'),
	(36, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 95.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', 'Windows 10', '2021-12-08 18:27:29'),
	(37, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 95.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', 'Windows 10', '2021-12-08 21:18:11'),
	(38, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 95.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', 'Windows 10', '2021-12-09 09:25:18'),
	(39, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 95.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', 'Windows 10', '2021-12-09 15:35:15'),
	(40, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 95.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', 'Windows 10', '2021-12-10 07:58:24'),
	(41, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 95.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', 'Windows 10', '2021-12-10 17:48:43'),
	(42, 1, '{"role":"1","roleText":"System Administrator","name":"Nina"}', '127.0.0.1', 'Firefox 95.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0', 'Windows 10', '2021-12-11 11:47:33');
/*!40000 ALTER TABLE `tbl_last_login` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_reset_password
DROP TABLE IF EXISTS `tbl_reset_password`;
CREATE TABLE IF NOT EXISTS `tbl_reset_password` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` bigint(20) NOT NULL DEFAULT 1,
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table nina.tbl_reset_password: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_reset_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_reset_password` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_roles
DROP TABLE IF EXISTS `tbl_roles`;
CREATE TABLE IF NOT EXISTS `tbl_roles` (
  `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  PRIMARY KEY (`roleId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table nina.tbl_roles: 3 rows
/*!40000 ALTER TABLE `tbl_roles` DISABLE KEYS */;
INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
	(1, 'System Administrator'),
	(2, 'Manager'),
	(3, 'Employee');
/*!40000 ALTER TABLE `tbl_roles` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_sesi_diagnosa
DROP TABLE IF EXISTS `tbl_sesi_diagnosa`;
CREATE TABLE IF NOT EXISTS `tbl_sesi_diagnosa` (
  `is` int(11) NOT NULL AUTO_INCREMENT,
  `sesi` varchar(50) DEFAULT NULL,
  `diagnosa_id` int(11) DEFAULT NULL,
  `kode_gejala` varchar(50) DEFAULT NULL,
  `jawaban` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`is`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table nina.tbl_sesi_diagnosa: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_sesi_diagnosa` DISABLE KEYS */;
INSERT INTO `tbl_sesi_diagnosa` (`is`, `sesi`, `diagnosa_id`, `kode_gejala`, `jawaban`) VALUES
	(1, 'e1dZe2', 1, 'G01', 'Tidak');
/*!40000 ALTER TABLE `tbl_sesi_diagnosa` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_users
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table nina.tbl_users: 3 rows
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `roleId`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
	(1, 'admin@example.com', '21232f297a57a5a743894a0e4a801fc3', 'Nina', '9890098900', 1, 0, 0, '2015-07-01 18:56:49', 1, '2020-04-26 16:50:18'),
	(2, 'manager@example.com', '21232f297a57a5a743894a0e4a801fc3', 'Manager', '9890098900', 2, 0, 1, '2016-12-09 17:49:56', 1, '2020-04-24 13:29:58'),
	(3, 'employee@example.com', '21232f297a57a5a743894a0e4a801fc3', 'Employee', '9890098900', 3, 0, 1, '2016-12-09 17:50:22', 1, '2020-04-24 13:29:54');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_vehicles
DROP TABLE IF EXISTS `tbl_vehicles`;
CREATE TABLE IF NOT EXISTS `tbl_vehicles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL COMMENT 'Foreign key from Customers table',
  `vehiclemake` varchar(100) NOT NULL,
  `vehiclemodel` varchar(100) NOT NULL,
  `vehiclemadeyear` varchar(100) NOT NULL,
  `vehicleregistrationnumber` varchar(100) NOT NULL,
  `vehiclenumber` varchar(100) NOT NULL,
  `ownername` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `gpskitinstalldate` date NOT NULL,
  `eminumber` varchar(100) NOT NULL,
  `gpskitmobilenumber` varchar(100) NOT NULL,
  `gpsdevicemodelnumber` varchar(100) NOT NULL,
  `erpportalassociation` varchar(100) NOT NULL,
  `erpportalusername` varchar(100) NOT NULL,
  `nextrenewaldate` date NOT NULL,
  `installbystaff` varchar(100) NOT NULL,
  `communication` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A=Active, D= Deactive',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  FULLTEXT KEY `vehiclenumber` (`vehiclenumber`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table nina.tbl_vehicles: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_vehicles` DISABLE KEYS */;
INSERT INTO `tbl_vehicles` (`id`, `cid`, `vehiclemake`, `vehiclemodel`, `vehiclemadeyear`, `vehicleregistrationnumber`, `vehiclenumber`, `ownername`, `address`, `gpskitinstalldate`, `eminumber`, `gpskitmobilenumber`, `gpsdevicemodelnumber`, `erpportalassociation`, `erpportalusername`, `nextrenewaldate`, `installbystaff`, `communication`, `created_at`, `updated_at`, `status`) VALUES
	(1, 15, 'Hyundai', 'Xcent', '2020', 'RGTRT202067890HGTFT', 'UP16FT 9450', 'Ajay Goyal', 'A 73 Sector 65 Noida UP', '2020-04-08', 'FTR3456OP90', '9873123490', 'GS23UI', 'WTGPS2020', 'up16ft9450', '2021-07-17', 'Sonu', 'GPS Setup', '2020-04-22 02:30:00', '2020-04-09 02:30:00', 'A'),
	(3, 42, 'Hyundai', 'Santro', '2019', 'RT2020GT4590FT', 'UP14EE6887', 'Vinita Dwivedi', '              Noida', '2020-04-23', 'GTEMI3450989090D', '9876453422', 'WT45', 'WTRetra56', 'up14ee6887', '2021-08-26', 'Guddu', '              test by Guddu', '2020-04-28 17:36:08', '2020-04-28 19:13:44', 'A');
/*!40000 ALTER TABLE `tbl_vehicles` ENABLE KEYS */;

-- Dumping structure for table nina.tbl_year_months
DROP TABLE IF EXISTS `tbl_year_months`;
CREATE TABLE IF NOT EXISTS `tbl_year_months` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` year(4) NOT NULL,
  `monthname` varchar(100) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1= Active 2=Deactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table nina.tbl_year_months: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_year_months` DISABLE KEYS */;
INSERT INTO `tbl_year_months` (`id`, `year`, `monthname`, `startdate`, `enddate`, `created_at`, `updated_at`, `status`) VALUES
	(1, '2020', 'January', '2020-01-01', '2020-01-31', '2020-04-04 18:15:29', '2020-04-04 18:15:29', 1),
	(2, '2020', 'February', '2020-02-01', '2020-02-29', '2020-04-04 18:15:29', '2020-04-04 18:15:29', 1),
	(3, '2020', 'March', '2020-03-01', '2020-03-31', '2020-04-04 18:16:15', '2020-04-04 18:16:15', 1),
	(4, '2020', 'April', '2020-04-01', '2020-04-30', '2020-04-04 18:16:15', '2020-04-04 18:16:15', 1),
	(5, '2020', 'May', '2020-05-01', '2020-05-31', '2020-04-11 01:55:13', '2020-04-11 01:55:13', 1),
	(6, '2020', 'June', '2020-06-01', '2020-06-30', '2020-04-11 01:55:13', '2020-04-11 01:55:13', 1);
/*!40000 ALTER TABLE `tbl_year_months` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
