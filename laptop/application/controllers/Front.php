<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Nina
 * @version : 1.1
 * @since : 05 Desember 2021
 */
class Front extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
	
	function generateRandomString($length = 25) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	public function newSesi(){
		$cook = $this->generateRandomString(6);
		setcookie("prevsesi", $_GET["prevsesi"], time() + (86400 * 30), "/"); 
		setcookie("sesi", $cook, time() + (86400 * 30), "/"); 
		setcookie("isFinished", "No", time() + (86400 * 30), "/");

		//data solusi
		$SQL = "SELECT A.*, B.solusi FROM tbl_sesi_diagnosa A 
				LEFT JOIN tbl_diagnosa B ON B.kode_gejala = A.kode_gejala
				WHERE A.sesi = '".$_GET["prevsesi"]."' AND A.jawaban = 'YA'";
				//die($SQL);
		$hasil = $this->db->query($SQL);
		$data["solusi"] = $hasil->result_array();
		
		$this->load->view("front_end/home", $data, 'refresh');
	}
	
	public function jawaban_save(){
		$this->db->select("COUNT(*) as sudahvote");
		$this->db->from("tbl_sesi_diagnosa");
		$this->db->where("sesi", $_COOKIE["sesi"]);
		$this->db->where("kode_gejala", $this->input->post("kode_gejala"));
		$hasil = $this->db->get()->row_array();
		if($hasil["sudahvote"] > 0 ){
			
			$data = array(
				'sesi' => $_COOKIE["sesi"],
				'diagnosa_id'	=> $this->input->post("jawaban_id"),
				'kode_gejala'	=> $this->input->post("kode_gejala"),
				'jawaban'	=> $this->input->post("jawab")
			);
			$this->db->where("sesi", $_COOKIE["sesi"]);
			$this->db->where("kode_gejala", $this->input->post("kode_gejala"));
			$this->db->update("tbl_sesi_diagnosa", $data);
			echo "Jawaban sudah terupdate.";
			
		} else {
		
			$data = array(
				'sesi' => $_COOKIE["sesi"],
				'diagnosa_id'	=> $this->input->post("jawaban_id"),
				'kode_gejala'	=> $this->input->post("kode_gejala"),
				'jawaban'	=> $this->input->post("jawab")
			);
			$this->db->insert("tbl_sesi_diagnosa", $data);
			echo "Jawaban sudah tersimpan.";
		}
	}
	
    public function index()
    {
		
		$data["sesi"] = $_COOKIE["sesi"];
		

		$start = $this->input->get("start") ? $this->input->get("start") : 0;
		
			
		if(!is_numeric($start) || $start < 0){
			header('location: '. base_url() .'front/finish');
		} else {
			$SQL = "SELECT * FROM tbl_diagnosa where isDeleted = 0 LIMIT 1 OFFSET ". $start;
			$hasil = $this->db->query($SQL);
			$dt = $hasil->row_array();
			$num = $hasil->num_rows();
			
			if($num > 0){
				
			} else {
				header('location:'. base_url() .'front/?start=end');
			}
			$data["diagnosa"] = $hasil->result_array();
			
			
			$SQL = "SELECT DISTINCT(jenis_kerusakan) FROM tbl_diagnosa where isDeleted = 0";
			$hasil = $this->db->query($SQL);
			$dt = $hasil->result_array();
			$data["jenis"] = $dt;
			
			$this->load->view("front_end/home", $data);
		}
		
    }
    
        
    function finish(){
		$this->load->view("front_end/home");
	}
	
        
    function getReport(){
		$this->load->view("front_end/report");
	}
}

?>