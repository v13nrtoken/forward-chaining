<HTML>
<HEAD>
<TITLE>Sistem Pakar Kerusakan Hardware Laptop</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<script src="<?php echo base_url();?>assets/jquery-3.6.0.min.js"></script>
<style type="text/css">

.style2 {font-size: 18px}
#tbjenis td {
    padding: 7px;
}
#tbdiagnosa td {
    padding: 7px;
}
</style>
<script>
	function getUrlVars() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
		function(m,key,value) {
		  vars[key] = value;
		});
		return vars;
	  }
	
	$(document).ready(function(){
		var jawaban = "";
		
		$("#jawaban").hide();
		
		$("#jawabya").click(function(){
			if ($("#jawabya").prop('checked')) {
				$("#jawaban").show();
				jawaban = "Ya";
				$("#submit").trigger("click");
				//$("#maju").trigger("click");
			}
		});
		$("#jawabtidak").click(function(){
			if ($("#jawabtidak").prop('checked')) {
				$("#jawaban").hide();
				
				jawaban = "Tidak";
				$("#submit").trigger("click");
				//$("#maju").trigger("click");
			}
		});
		$("#maju").click(function(){
			var fType = getUrlVars()["start"] ? getUrlVars()["start"] : 0;
			nomoritem = parseInt(fType)+1;
					
					window.location.href = "<?php echo base_url();?>front/?start="+nomoritem;
			
		});
		$("#submit").click(function(){
			
			
			$.ajax({
				url: "<?php echo base_url();?>front/jawaban_save",
				type: "post",
				dataType: "html",
				data: {
					jawaban_id: $("#jawaban_id").val(),
					jawab: jawaban,
					kode_gejala : $("#kode_gejala").val()
				},
				success: function(res){
					alert(res);
					
				}
			});
			
		});
		$("#kembali").click(function(){
			var fType = getUrlVars()["start"] ? getUrlVars()["start"] : 0;
			nomoritem = parseInt(fType)-1;
			window.location.href = "<?php echo base_url();?>front/?start="+nomoritem;
		});
		$("#kembalikeDg").click(function(){
			history.back();
		});
		$("#selesaiDiagnosa").click(function(){
			prev_session = '<?php echo $_COOKIE["sesi"];?>';
			window.location.href = "newSesi?prevsesi="+prev_session;
		});
	});
	
$(document).ready(function(){    
    //Check if the current URL contains '#'
    if(document.URL.indexOf("#")==-1){
        // Set the URL to whatever it was plus "#".
        url = document.URL+"#";
        location = "#";

        //Reload the page
        location.reload(true);
    }
});
</script>
</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
<!-- ImageReady Slices (frontEnd.psd) -->
<TABLE WIDTH=1000 BORDER=0 CELLPADDING=0 CELLSPACING=0 align="center">
	<TR>
		<TD COLSPAN=8>
			<IMG SRC="<?php echo base_url();?>images/assets_01.gif" WIDTH=967 HEIGHT=213 ALT=""></TD>
		<TD ROWSPAN=5>
			<IMG SRC="<?php echo base_url();?>images/assets_02.gif" WIDTH=33 HEIGHT=600 ALT=""></TD>
	</TR>
	<TR>
		<TD COLSPAN=2>
			<a href="<?php echo base_url();?>"><IMG SRC="<?php echo base_url();?>images/assets_03.gif" WIDTH=180 HEIGHT=27 ALT=""></a></TD>
		<TD>
			<a href="<?php echo base_url();?>front/index/jenis_kerusakan"><IMG SRC="<?php echo base_url();?>images/assets_04.gif" WIDTH=163 HEIGHT=27 ALT=""></a></TD>
		<TD>
			<a href="<?php echo base_url();?>"><IMG SRC="<?php echo base_url();?>images/assets_05.gif" WIDTH=130 HEIGHT=27 ALT=""></a></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/assets_06.gif" WIDTH=105 HEIGHT=27 ALT=""></TD>
		<TD>
			<a href="<?php echo base_url();?>login" target="_BLANK"><IMG SRC="<?php echo base_url();?>images/assets_07.gif" WIDTH=95 HEIGHT=27 ALT=""></a></TD>
		<TD COLSPAN=2>
			<IMG SRC="<?php echo base_url();?>images/assets_08.gif" WIDTH=294 HEIGHT=27 ALT=""></TD>
	</TR>
	<TR>
		<TD ROWSPAN=2>
			<IMG SRC="<?php echo base_url();?>images/assets_09.gif" WIDTH=31 HEIGHT=286 ALT=""></TD>
		<TD COLSPAN=7>
			<IMG SRC="<?php echo base_url();?>images/assets_10.gif" WIDTH=936 HEIGHT=12 ALT=""></TD>
	</TR>
	<TR>
		<TD COLSPAN=6 valign="top">
		<?php
			if($this->uri->segment(2)!="finish" && $this->uri->segment(2)!="newSesi" && $this->uri->segment(3)!="jenis_kerusakan"){
		?>	
					<table width="100%" border="0" id="konten">
						  <tr>
							<td colspan="2">&nbsp;</td>
							<td width="22%">&nbsp;</td>
							<td width="19%">&nbsp;</td>
						  </tr>
						  <tr>
							<td height="34" colspan="2">JAWABLAH PERTANYAAN BERIKUT : </td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td height="40" colspan="4" bgcolor="#39BEC6"><span class="style2">
							Apakah Anda mengalami : 
							<?php
								foreach($diagnosa as $v){
							?>
								
								<?php echo $v["gejala"];?>
								
							<?php } ?>
							? 
							</span></td>
						  </tr>
						  <tr>
							<td width="15%"><label>
							  <input name="radiobutton" type="radio" id="jawabya" value="radiobutton" >
				Benar (YA)</label></td>
							<td width="44%"><input name="radiobutton" id="jawabtidak"  type="radio" value="radiobutton">
				Salah (TIDAK)</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td colspan="2"><label>
							  <input style="display:none;" type="button" name="submit" id="submit" value="Jawab">
							</label></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td colspan="3">Jawaban yang Anda Pilih : <span class="style2">
							
							<?php
								foreach($diagnosa as $v){
							?>
								
								<span id="jawaban"><?php echo $v["gejala"];?></span>
								<input type="hidden" id="jawaban_id" value="<?php echo $v["id"];?>">
								
								<input type="hidden" id="kode_gejala" value="<?php echo $v["kode_gejala"];?>">
								
							<?php } ?>
							
							</span></td>
						  </tr>
						  <tr>
							<td colspan="2">&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td colspan="2"><input type="submit" name="Submit2" value="Back" id="kembali">
							<input type="submit" name="Submit3" value="Next" id="maju"></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td colspan="2">
							</td>
							<td>&nbsp;</td>
							<td align="right"><small>Sesi = <?php echo $sesi;?></small>&nbsp;&nbsp;&nbsp;</td>
						  </tr>
						</table>
			
			<?php } elseif($this->uri->segment(2)=="newSesi"){?> 

					Reporting <?php echo $_COOKIE["prevsesi"];?><br>
					
					<table border=1 id="tbdiagnosa" style="border-collapse:collapse;">
						<tr>
							<td>Nomor</td>
							<td>Solusi</td>
						</tr>
						<?php $no= 0; foreach($solusi as $v) { ?>
						
						
						<tr>
							<td><?php echo ++$no;?></td>
							<td><?php echo $v["solusi"];?></td>
						</tr>
						
						
						<?php } ?>
					</table>

			<?php } elseif($this->uri->segment(3)=="jenis_kerusakan"){?> 

					Jenis Kerusakan <br>
					
					<table border=1 id="tbjenis" style="border-collapse:collapse;">
						<tr>
							<td>Nomor</td>
							<td>Jenis</td>
							<td></td>
						</tr>
						<?php $no= 0; foreach($jenis as $v) { ?>
							
							
							<tr>
								<td><?php echo ++$no;?></td>
								<td><?php echo $v["jenis_kerusakan"];?></td>
								<td align="center"><a href="#">Pilih</a></td>
							</tr>
							
							
							<?php } ?>
					</table>

			<?php } else { ?>
						<table width="100%">
							<tr>
								<td>Finish ?</td>
							</tr>
							<tr>
								<td><input type="button" value="Ya" id="selesaiDiagnosa">&nbsp;&nbsp;&nbsp;<input type="button" id="kembalikeDg" value="Tidak"></td>
							</tr>
						</table>
			<?php } ?>
		</TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/assets_12.gif" WIDTH=26 HEIGHT=274 ALT=""></TD>
	</TR>
	<TR>
		<TD COLSPAN=8>
			<IMG SRC="<?php echo base_url();?>images/assets_13.gif" WIDTH=967 HEIGHT=74 ALT=""></TD>
	</TR>
	<TR>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=31 HEIGHT=1 ALT=""></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=149 HEIGHT=1 ALT=""></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=163 HEIGHT=1 ALT=""></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=130 HEIGHT=1 ALT=""></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=105 HEIGHT=1 ALT=""></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=95 HEIGHT=1 ALT=""></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=268 HEIGHT=1 ALT=""></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=26 HEIGHT=1 ALT=""></TD>
		<TD>
			<IMG SRC="<?php echo base_url();?>images/spacer.gif" WIDTH=33 HEIGHT=1 ALT=""></TD>
	</TR>
</TABLE>
<!-- End ImageReady Slices -->
</BODY>
</HTML>