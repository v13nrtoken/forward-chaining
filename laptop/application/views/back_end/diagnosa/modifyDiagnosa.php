<?php
$id = $userInfo->id;
$kode_kerusakan = $userInfo->kode_kerusakan;
$jenis_kerusakan = $userInfo->jenis_kerusakan;
$kode_gejala = $userInfo->kode_gejala;
$gejala = $userInfo->gejala;
$solusi = $userInfo->solusi;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Diagnosa Management
        <small>Edit Diagnosa Informations</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Diagnosa Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editDiagnosa" method="post" id="editUser" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Kode Kerusakan</label>
                                        <input type="hidden" class="form-control required" value="<?php echo $id;?>" id="idDiagnosa" name="idDiagnosa" maxlength="128">
                                        <input type="text" class="form-control required" value="<?php echo $kode_kerusakan;?>" id="kode_kerusakan" name="kode_kerusakan" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Jenis Kerusakan</label>
                                        <input type="text" class="form-control required" id="jenis_kerusakan" value="<?php echo $jenis_kerusakan;?>" name="jenis_kerusakan" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Kode Gejala</label>
                                        <input type="text" class="form-control required" id="kode_gejala" name="kode_gejala" value="<?php echo $kode_gejala;?>" maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Gejala</label>
                                        <textarea  class="form-control required " id="gejala" name="gejala" ><?php echo $gejala;?></textarea>
                                    </div>
                                </div>
                            </div>
                                                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="password">Solusi</label>
                                        <textarea class="form-control required" id="solusi" name="solusi"><?php echo $solusi;?></textarea>
                                    </div>
                                </div>
                               
                            </div>
                            
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>